from django.shortcuts import render
from django.shortcuts import render, HttpResponse
from MobikeApp.forms import FormularioLogin

# Create your views here.

def inicio(request):
    
    if request.method == "POST":
        miFormulario = FormularioLogin(request.POST)

        if miFormulario.is_valid():

            form=miFormulario.cleaned_data

            if form.get('usuario') == "admin":
                return render(request, "mobikeapp/vistaAdmin.html")

            elif form.get('usuario') == "funcionario":
                return render(request, "mobikeapp/vistaFuncionario.html")

            elif form.get('usuario') == "usuario":
                return render(request, "mobikeapp/vistaUsuario.html")
            
            else:
                return render(request, "mobikeapp/inicioMal.html")


                

    else:
        miFormulario = FormularioLogin()
        return render(request, "mobikeapp/inicio.html", {"form": miFormulario})

        

